#include <string>
#include <list>
#include <time.h>
#include <iostream>
#include <limits>

using namespace std;
/*
Kurzbeschreibung des Programms:
    Das Programm soll 5 mal folgenden durchlauf generieren:
        1. Zufallszahlen zwischen 0 und 1 generieren bis die Zufallszahl > 0.85 ist und die Zahlen speichern.
        2. Aus den generierten Zahlen den Mittelwert, das Minimum und Maximum berechnen
        3. alle Ergebnisse auf der Console ausgeben.

 Autor: {Julian Hahn, Nick Löewicke}, {9331133,}, Laborgruppe, Team
 Datum: 20.03.2020
 Version: 1.0 (bei höheren Versionen 1.1 u.s.w.)
*/


int main()
{
    //durchlaufe 5 iterationen
    srand( time(NULL) );
    for (int i = 0; i < 5; i++)
    {
        //gebe die Iteration an:
        cout << "Iteration: " << i + 1 << endl;

        list <float> numbers;
        float current = 0;
        //iteriere solange die Zahl nicht größer 0.85 ist
         while (current < 0.85){
            //generiere Zahlen zwischen 0 und 1
            current = static_cast<double>(rand())/RAND_MAX;
            numbers.push_front(current);
        }

        //die ziffer die zum abbruch geführt hat noch hinzufügen
        //numbers.push_back(current);

        //nachdem die while vorbei ist, können wir die Werte berechnen
        //min als FLT_MAX also die größte float zahl, damit die erste Zahl auch immer kleiner ist als der initialisierungswert
        float sum = 0, max = 0, min = numeric_limits<float>::max();
        cout << "Die Zahlenfolge lautet: ";
        for(int i = 0; i < numbers.size(); i++){
            float tmp;
            tmp = numbers.front();
            numbers.pop_front();

            //add to sum
            sum += tmp;

            //check if new min
            if (tmp < min)
            {
                min = tmp;
            }

            //check if new max
            if (tmp > max)
            {
                max = tmp;
            }
         cout << tmp <<", ";
        }
        //Zeile mit den Zahlen beenden
        cout << endl;

        //sum, max und min ausgeben
        cout << "Die Summe ist: " << sum << endl;
        cout << "Das Minimum ist: " << min << endl;
        cout << "Das Maximum ist: " << max << endl;

        //leerzeile
        cout << endl;
    }

    return 0;
}
