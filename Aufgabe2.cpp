#include <string>
#include <list>
#include <time.h>
#include <iostream>
#include <limits>
#include <string>

using namespace std;
/*
Kurzbeschreibung des Programms:
 Aufgabenteil a) gebe die Anzahl an Bytes aller elementaren Datentypen an.
 Aufgabenteil b) die struct

 Autor: {Julian Hahn, Nick Löewicke}, {9331133,}, Laborgruppe, Team
 Datum: 20.03.2020
 Version: 1.0 (bei höheren Versionen 1.1 u.s.w.)
*/

int main(int argc, char const *argv[])
{
    //gebe die größe aller elementaren Datentypen aus.
    cout << "Bytes fuer einen Char: " << sizeof(char) << endl;
    cout << "Bytes fuer einen int: " << sizeof(int) << endl;
    cout << "Bytes fuer einen float: " << sizeof(float) << endl;
    cout << "Bytes fuer einen boolean: " << sizeof(bool) << endl;
    cout << "Bytes fuer einen double: " << sizeof(double) << endl;
    cout << "Bytes fuer einen long double: " << sizeof(long double) << endl;

    //erstellt ein Struktur "student"
    struct student
    {
        long int id;
        string name;
        string studiengang;
        float note;
        bool bestanden;
    };

    //den benötigten Speicherplatz für die Struktur ausgeben
    cout << sizeof(student) << endl;
    return 0;
}
